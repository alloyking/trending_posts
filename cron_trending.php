<?php

/* external interface for a curl/cron request.
The pupose of this script is to populate the blogs trending post data to memecache
so that it can be called on the website w/o running the query each time
*/



class cron_trending{


	public function __construct() {
		//add the external api call that curl can connect to
        add_action('wp_ajax_nopriv_populate_trending_post_data', array($this, 'populate_trending_post_data'));
    }

    public function populate_trending_post_data(){
        
    	//this key must be added to curl request
        $shared_user_agent = 'PUTAKEYHERE';

        $whitelisted_ips = array( //IPs allowed to run this operation
            '192.168.1.1',
            '127.0.0.1'
        );

        $request_user_agent = $_SERVER['HTTP_USER_AGENT'];
        $request_ip = $_SERVER['REMOTE_ADDR'];


        # Authenticate
        if($request_user_agent === $shared_user_agent && in_array($request_ip, $whitelisted_ips))
            echo $this->get_trending_posts(); 
        else
            echo 'Authentication failed for trending cron.';
        exit;
    }

    private function get_all_blogs(){
    	global $wpdb;
    	$blogs = $wpdb->get_results( "SELECT blog_id from wp_blogs" );
    	return $blogs;
    }

    private function get_trending_posts(){
    	global $nwm_trending_posts;
    	foreach ( $this->get_all_blogs()  as $blog ) {
			$id = $blog->blog_id;
			
    		//run wp query from NWM_Trending_Posts class, switch blog to obtain the results for each blog;
    		switch_to_blog($id);
            
            //lets only save 4 posts as The UI (sidebar) only calls for 4 at the moment
    		$this->set_blog_results($nwm_trending_posts->get_all_posts(4), $id);
    		restore_current_blog();
		}	
    }

    private function set_blog_results($posts, $blogid){	
		if(class_exists('Memcache')){
    		$memcache = memcache_connect('localhost', 11211);
			$memory_store = $memcache->set("trending_posts_".$blogid, $posts);
			//print_r($memcache->get("trending_posts_".$blogid));
		}
    }
}