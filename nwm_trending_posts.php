<?php
/*
Plugin Name: Trending Posts
Description: Plugin to track popular posts.
Version: 2.0
Author: Morsekode - Tim Shultis
*/

require_once 'cron_trending.php';

$nwm_trending_posts = new NWM_Trending_Posts();
$nwm_trending_cron = new cron_trending();

class NWM_Trending_Posts {

	const TRENDING_META_KEY = 'trending_views_count';
	const IS_SINGLE = 'is_single';
	const POST_ID = 'post_id';
	const TRENDING_RANGE = "now -2 weeks";

	public function __construct() {
		
		/*
		** if you are not using a cached site and ajax page transitions
		** enabling the below two action hooks and disabling the ajax tracking will likely work for you.
		*/
		//add auto track - works for non-cached sites only
		//add_action( 'wp_head', array($this, 'track_post'));
		////remove prefetching - screws with count.
		//remove_action( 'wp_head', array($this,  'adjacent_posts_rel_link_wp_head'), 10, 0);
		 
		

		//Add ajax tracking for visitors and logged-in users - recommended for cached sites
		add_action( 'wp_ajax_ajax_track_post', array( $this, 'ajax_track_post' ) );
		add_action( 'wp_ajax_nopriv_ajax_track_post', array( $this, 'ajax_track_post' ) );

		//custom action for adding javscript to the document.  
		//!!!! You would likely call this from single.php!!!!!!
		add_action( 'tracking_object', array( $this, 'tracking_object_callback' ) );
	}


	public function tracking_object_callback() {
		//this really is not needed if W3 DOES NOT exist.
		if ( !defined( 'W3TC' ) ) return ;

		//We have W3 installed so do the below
		global $post;
		$cat_names;
		$categories = get_the_category( $post->ID );
		foreach ( $categories as $category ) {
			$cat_names[] = array(
				'category_name' => $category->name
			);
		}

		$script = '<script type="text/javascript">';
		$script .= 'var tracking_object = {"'.self::POST_ID.'": '.( $post->ID ? $post->ID : 0 ).', "'.self::IS_SINGLE.'":'.( is_single() == 1 ? 1 : 0 ).', "cats": '.json_encode( $cat_names ).'};';
		$script .= '</script>';
		echo $script;
	}


	private function set_post_view( $postID ) {
		$trend_data = $this->get_post( $postID );
		if ( $trend_data ) {
			$trend_data = json_decode( $trend_data, true );
		}
		$trend_data[] = array( date( 'Y-m-d' ) );
		update_post_meta( $postID, self::TRENDING_META_KEY, json_encode( $trend_data ) );
	}

	//auto fired by action wp_head
	//we do not run this is Total Cache exists as it will not work
	public function track_post( $post_id ) {
		//exit out if W3 (use ajax instead)
		if ( defined( 'W3TC' ) ) return;

		if ( !is_single() ) return;
		if ( empty ( $post_id ) ) {
			global $post;
			$post_id = $post->ID;
		}
		$this->set_post_view( $post_id );
	}

	//Called via ajax to track a post.
	//We call this from the sites javascript file(s) *main.js in MK's case
	public function ajax_track_post() {
		$post_id = $_POST[self::POST_ID];
		if ( $_POST[self::IS_SINGLE] != 1 ) return;
		$this->set_post_view( $post_id );
		die( 'tracked' );
	}

	public function get_all_posts_cache( $limit=null, $blogid=null ) {
		if ( !$blogid ) {
			global $blog_id;
			$blogid = $blog_id;
		}
		//This is set-up to utilize memcache as a way to present the results to the user.
		//Not leaving a fallback for non-memcache servers, as it is pretty disruptive to call this query on every single page view for the a user
		if ( class_exists( 'Memcache' ) && class_exists( 'cron_trending' ) ) {
			$memcache = memcache_connect( 'localhost', 11211 );
			if ( $limit ) {
				$trend_data = $memcache->get( "trending_posts_".$blogid );
				if ( $trend_data ) {
					return array_slice( $trend_data, 0 , $limit );
				} else {
					//if no data in the cache exists, just pull from the db
					return $this->get_all_posts( $limit );
				}
			} else {
				$trend_data = $memcache->get( "trending_posts_".$blogid );
				if ( $trend_data ) {
					return $memcache->get( $trend_data );
				} else {
					//if no data in the cache exists, just pull from the db
					return $this->get_all_posts();
				}
			}
		}
	}

	//Returns posts with a Trending Count saved in postmeta. Const TRENDING_RANGE will crontrol how far back this query goes.
	public function get_all_posts( $limit=null ) {
		global $wp_query, $wpdb;

		$trending_posts = array();

		$trendrows = $wpdb->get_results( "SELECT $wpdb->posts.ID, $wpdb->postmeta.meta_value FROM $wpdb->posts INNER JOIN $wpdb->postmeta ON $wpdb->postmeta.post_id = $wpdb->posts.ID
			WHERE $wpdb->posts.post_status = 'publish' && $wpdb->posts.post_type = 'post' && $wpdb->postmeta.meta_key = '".self::TRENDING_META_KEY."'" );

		if ( $trendrows ) {
			foreach ( $trendrows as $trendrow ) {
				$post_id = $trendrow->ID;

				$trending_meta = $trendrow->meta_value;
				//$trending_meta = '[["2013-11-07"],["2013-11-07"],["2013-11-07"],["2013-11-08"],["2013-11-08"],["2013-11-09"]]';
				//if a trending posts contain a date that is within the accepted date range count them.
				if($trending_meta){
					$trend_views = count( array_intersect( $this->flatten_array( json_decode( $trending_meta, true ) ), $this->date_range() ) );
					if ( $trend_views > 0 ) {
						$trending_posts[] = array(
							'post_id' => $post_id,
							'views' => $trend_views
						);
					}
				}
			}

			usort( $trending_posts, $this->view_sorter( 'views' ) );
			if ( $limit ) {
				//slice the results in order to satisfy the limit
				return array_slice( array_reverse( $trending_posts ), 0, $limit );
			} else {
				return array_reverse( $trending_posts );
			}
		}
	}

	//get trending data for a single post
	public function get_post( $post_id ) {
		if ( !$post_id ) {
			global $post;
			$post_id = $post->ID;
		}
		return get_post_meta( $post_id, self::TRENDING_META_KEY, true );
	}

	//set a start date and end date -  returns array a 1-week<default> span of dates between ( array('start', 'between', 'end'); )
	private function date_range( $startDate=null, $endDate=null, $range=null ) {

		if ( !$range ) {
			if ( self::TRENDING_RANGE ) {
				$range = self::TRENDING_RANGE;
			} else {
				$range = 'now -1 weeks';
			}
		}

		if ( !$startDate ) {
			$startDate = new DateTime( date( "Y-m-d" , strtotime( self::TRENDING_RANGE ) ) );
		}
		if ( !$endDate ) {
			$endDate = new DateTime( date( "Y-m-d" ) );
		}


		$endDateInt = new DateInterval( "P1D" );
		$endDate->add( $endDateInt );

		$period = new DatePeriod( $startDate, new DateInterval( 'P1D' ), $endDate );

		foreach ( $period as $dt ) {
			$dateRange[] = $dt->format( "Y-m-d" );
		}

		return $dateRange;
	}

	//flattens an array
	private function flatten_array( array $array ) {
		$return = array();
		array_walk_recursive( $array, function( $a ) use ( &$return ) { $return[] = $a; } );
		return $return;
	}

	private function view_sorter( $key ) {
		return function ( $a, $b ) use ( $key ) {
			//Natural order string comparison
			return strnatcmp( $a[$key], $b[$key] );
		};
	}
}