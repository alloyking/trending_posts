var trending_post = function() {
    if (typeof tracking_object !== 'undefined' && tracking_object !== null) {
        
        var track_cats = [];
       
        jQuery.each(tracking_object.cats, function() {
            //we only have 2 categories to track, so just looking for those
            if (this.category_name == "CAT1" || this.category_name == "CAT2") {
                track_cats.push(this.category_name);
            }
        });

        if (track_cats.length !== 0) {
            jQuery.ajax({
                url: '' + SITE + '/wp-admin/admin-ajax.php',
                type: "POST",
                data: {
                    action: 'ajax_track_post',
                    post_id: tracking_object.post_id,
                    is_single: tracking_object.is_single
                },

                success: function(data, textStatus, XMLHttpRequest) {
                    //we are on a single page  ajax app, we need to null this out, or we end up with dbl tracked posts
                    //placing the tracking object in the header will fail as it will not be updated on each ajax content swap.
                    tracking_object = null;
                }
            });
        }
    }
};

trending_post();