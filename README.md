# Trending Posts #

Track the viewing of posts on a Wordpress site.  This plugin is used to track the view of posts in a particular category.  An ajax call is made to php, which updates postmeta.  

We also use this to display a sidebar calling out the most popular posts in the last two weeks.  Since the data is serialized and rather processor intensive to sort, we cache the results in memcache before displaying on the front end.   Cron is utilized to run the query and populate the cache.